db.fruits.aggregate([
		{
			$match: {supplier: {$regex: `Red Farms Inc.`, $options: `$i`}}
		},
		{
			$count: `Total Items`
		}
	])

db.fruits.aggregate([
		{
			$match: {price: {$gt: 50}}
		},
		{
			$count: `Total:`
		}
	])

db.fruits.aggregate([
		{
			$match: {onSale: true}
		},
		{
			$group: {_id: `$supplier`, avg_price: {$avg: `$price`}}
		}
	])

db.fruits.aggregate([
		{
			$match: {onSale: true}
		},
		{
			$group: {_id: `$supplier`, highest_price: {$max: `$price`}}
		}
	])

db.fruits.aggregate([
		{
			$match: {onSale: true}
		},
		{
			$group: {_id: `$supplier`, lowest_price: {$min: `$price`}}
		}
	])